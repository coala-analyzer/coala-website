<button onclick="toggle_navi()" class="menubutton">Menu</button>
<ul class="navi" id="navi">
    <li><?php echo _("Home") ?></li>
    <li><?php echo _("Documentation") ?></li>
    <li><?php echo _("FreeBear") ?></li>
    <li><?php echo _("Wiki") ?></li>
    <li><?php echo _("Reporting Bugs") ?></li>
    <li><?php echo _("Getting Involved") ?></li>
</ul>
