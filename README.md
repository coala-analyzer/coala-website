coala-website
=============

This repository contains the website for the coala project:
http://github.com/sils1297/coala

A live version of this website can be found at:
http://coala.schuirmann.net/
although this is not guaranteed to be the latest version.

Status
======
[![Code Climate](https://codeclimate.com/github/sils1297/coala-website/badges/gpa.svg)](https://codeclimate.com/github/sils1297/coala-website)

This project is heavy WIP. Look at the issues to get more information at what is planned.
