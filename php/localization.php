<?php
/* License: GPLv3 */

session_start();

$get = filter_input(INPUT_GET, "locale", FILTER_SANITIZE_STRING);
$cookie = filter_input(INPUT_COOKIE, "locale", FILTER_SANITIZE_STRING);

if (!empty($get)) {
    $locale = disallow_path_change($get);
    // Expires in one year
    setcookie("locale", $locale, time() + 31536000);
} else if (!empty($cookie)) {
    $locale = disallow_path_change($cookie);
} else {
    $locale = "en_US";
}

putenv("LANG=".$locale);
setlocale(LC_ALL, $locale);

$domain = "webcoala";
bindtextdomain($domain, "./locale");
bind_textdomain_codeset($domain, 'UTF-8');
textdomain($domain);
