<?php

/**
 * Removes all dots and slashes from the string so the string cant do harm to
 * paths.
 *
 * @param type $str the string to secure
 */
function disallow_path_change($str) {
    return str_replace(array(".", "\\", "/"), "", $str);
}
