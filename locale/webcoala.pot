# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-11-04 12:11+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: index.php:31
msgid "Welcome to coala!"
msgstr ""

#: navi.php:3
msgid "Home"
msgstr ""

#: navi.php:4
msgid "Documentation"
msgstr ""

#: navi.php:5
msgid "FreeBear"
msgstr ""

#: navi.php:6
msgid "Wiki"
msgstr ""

#: navi.php:7
msgid "Reporting Bugs"
msgstr ""

#: navi.php:8
msgid "Getting Involved"
msgstr ""
