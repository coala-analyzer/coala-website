<?php
/* License: GPLv3 */

ini_set('display_errors', 'On');
error_reporting(E_ALL);

include './php/utils.php';
include './php/localization.php';

?>

<!DOCTYPE html>
<html>
    <head>
        <title>coala</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" type="text/css" href="css/webcoala.css" />
        <script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/webcoala.js"></script>
    </head>
    <body>
        <div class="content-wrapper">
        <div class="header"><?php

include './logo.html';
include './navi.php';

?></div>
        <div class="content"><?php

echo _("Welcome to coala!");

?></div>
        <div class="footer">TODO Footer</div>
        </div>
    </body>
</html>
